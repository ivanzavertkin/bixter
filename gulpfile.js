var gulp            = require('gulp'),
    scss            = require('gulp-sass'),
    browserSync     = require('browser-sync'),
    autoprefixer    = require('gulp-autoprefixer'),
    livereload      = require('gulp-livereload');


/*TASK*/
gulp.task('scss', function () {
    return gulp.src('dev/static/scss/main.scss')
        .pipe(scss())
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dev/static/css'))
        .pipe(browserSync.reload({stream: true}))
        .pipe(livereload());
});

gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: 'dev'
        },
        notify: false
    })
});

gulp.task('watch',['browser-sync', 'scss'], function(){
    livereload.listen();
    gulp.watch('dev/static/scss/*.scss', ['scss'], browserSync.reload);
    gulp.watch('dev/index.html', browserSync.reload);
    gulp.watch('dev/static/js/*.js', browserSync.reload);
});